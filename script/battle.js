const mappToData = (data,field) => {
    let mapping = { },max = { maxNum:0,name:''}
        data.map(item => {
            if(!mapping[item[field]]){
                mapping[item[field]] = 1
            }else{
                mapping[item[field]] = mapping[item[field]] + 1
            }

            if(mapping[item[field]] > max.maxNum){
                max.maxNum = mapping[item[field]]
                max.name = item[field]
            } 
        })
        if(field === "attacker_outcome"){
            return mapping
        }else{
            return max.name;
        }
        
}


const getBattlesData = async () => {

    try {
        const response = await fetch('https://api.jsonbin.io/b/5f917576adfa7a7bbea6cccf')
        const data = await response.json()

        const attackerKing = mappToData(data,"attacker_king")
    
        const defenderKing = mappToData(data,"defender_king")
    
        const region = mappToData(data,"region")
    
        const battleName =  mappToData(data,"name")
    
        mapping = mappToData(data,"attacker_outcome")
    
        let battleType = []
            data.map(item => {
                if(!battleType.includes(item.battle_type)){
                    battleType.push(item.battle_type)
                }
            })
    
        const defenderSize = []
            data.map(item => {
                if(item.defender_size){
                    defenderSize.push(item.defender_size)
                }
            })
    
    
        const maxDefenderSize = Math.max(...defenderSize)
        const minDefenderSize = Math.min(...defenderSize)
    
        const sumOfDefenderSize = defenderSize.reduce((current,total) => current+total)
        const avgDefenderSize = sumOfDefenderSize/data.length
    
    
        const result = 
            `{
                "most_active" : {
                    "attacker_king":${attackerKing},
    
                    "defender_king":${defenderKing},
    
                    "region":${region},
    
                    "name": ${battleName}
                },
                attacker_outcome:{
                    "win": ${mapping.win},
    
                    "loss": ${mapping.loss}
                },
                battle_type:${battleType}
                defender_size:{
                    "avg": ${avgDefenderSize},
    
                    "min": ${minDefenderSize},
                    
                    "max": ${maxDefenderSize}
                }
            }`
    
        document.getElementById("battlesData").innerHTML = result
    } catch (err) {
        console.log(err)
    }
   

}
