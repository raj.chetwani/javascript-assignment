const getData = async () => {

    try {
    const search = document.getElementById('search').value

    const response = await fetch(`https://api.github.com/search/repositories?q=${search}`)
    const data = await response.json()

    data.items.map( async (item) => {

        let result = { } 

        const user = await fetch(`${item.owner.url}`)
        const userInfo = await user.json()
        result.followers = userInfo.followers
        result.following = userInfo.following
        result.name = userInfo.name

        const branch = await fetch(`https://api.github.com/repos/${item.full_name}/branches`)
        const branchInfo = await branch.json()
        result.branchCount = branchInfo.length;


       const para = document.createElement("div");  
       para.style = 'margin-top:1vw;margin-bottom:1vw'  


       const output = `{
                         "name":${item.name},
                         "full_name":${item.full_name},
                         "private":${item.private},
                         "Owner": {
                             "login" : ${item.owner.login},
                             "name" : ${result.name}
                             "followers" : ${result.followers}
                             "following" :  ${result.following}
                         }
                         "licenseName": ${item.license.name},
                         "score": ${item.score}
                         "countOfBranch": ${result.branchCount}
                     }`


       para.innerHTML = output
                                
       document.getElementById("githubData").appendChild(para); 

     })
    } catch (err) {
        console.log(err);
    }
}

